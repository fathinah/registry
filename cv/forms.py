from django import forms
from .models import Pasien
import datetime
GEEKS_CHOICES =( 
    ("pr", "Perempuan"), 
    ("lk", "Laki-laki"), 
    ("-", "-"),  
) 

class PasienForm(forms.ModelForm):
    nama = forms.CharField(widget=forms.TextInput(attrs={"placeholder": "Nama pasien"}))
    usia = forms.CharField(widget=forms.TextInput(attrs={}))
    gender = forms.ChoiceField(choices = GEEKS_CHOICES) 
    ttl = forms.DateField(input_formats=['%Y-%m-%d'], initial=datetime.date.today(), widget=forms.TextInput(attrs={'class': 'form-control'}))
    dokter = forms.CharField(widget=forms.TextInput(attrs={}))
    class Meta:
        model = Pasien
        fields = ['nama','usia','gender','ttl','dokter']
