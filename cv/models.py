from django.db import models
from datetime import datetime,date

# Create your models here.
class Pasien(models.Model):
    nama = models.CharField(max_length=100)
    usia = models.IntegerField(max_length=30)
    gender = models.CharField(max_length=5)
    ttl = models.DateField()
    dokter = models.CharField(max_length=50)

    def __str__(self):
        return "{}.{}".format(self.id, self.nama)

