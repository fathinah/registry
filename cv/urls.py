from django.urls import path
from . import views
urlpatterns = [
    path('', views.form, name='form'),
    path('show/', views.showform, name='showform'),
    path('delete-activity/<int:pk>/', views.delete_activity, name='delete_activity'),

]