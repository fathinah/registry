from django.shortcuts import render,get_object_or_404, redirect
from .models import Pasien
from .forms import PasienForm
from django.contrib import messages

# Create your views here.
def showform(request):
    if request.method == 'GET':
        posts = Pasien.objects.all()
        return render(request, 'registry.html', {'posts':posts})

def delete_activity(request, pk):
    activity = get_object_or_404(Pasien, pk=pk)
    activity.delete()

    return redirect('showform')

def form(request):
    if request.method == "POST":
        data = PasienForm(request.POST)
        if data.is_valid():
            data.save()
            return redirect('showform')
        else:
            messages.warning(request, 'Data input is not valid, Please try again!')
    else:
        data = PasienForm()

    return render(request, 'isi_registry.html', {'form': data})
    
